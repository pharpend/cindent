build.sh - build CIndent
========================

Currently I only have examples (i.e. no actual code). Nonetheless,
documentation!

    clean                               Remove the out/ directory, then run.
    cleanwith COMMAND                   Run clean, then COMMAND.
    compile_examples                    Build the examples, will output to the out/ directory
    list_examples                       List the available examples.
    run_example AUTHOR/EXAMPLE_NAME     Run the example in examples/AUTHOR/EXAMPLE_NAME.c. Use list_examples to get a list.
    {help,--help,-h}                    Show this help.

Copyright (C) 2015 Peter Harpending. <peter@harpending.org>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice and this
notice are preserved.  This file is offered as-is, without any warranty.

