# CIndent - C pretty printer

This is the analog of [HIndent](https://github.com/chrisdone/hindent/), but for
C. I haven't written any of it yet. CIndent is licensed under the GNU General
Public License, version 3.

Use the included `build` file to compile CIndent.

Copyright (C) 2015 Peter Harpending. <peter@harpending.org>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice and this
notice are preserved.  This file is offered as-is, without any warranty.
