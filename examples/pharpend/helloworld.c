/* Hello, world, written in pharpend style.
 * Copyright (C) 2015 Peter Harpending. <peter@harpending.org>
 * 
 * Copying and distribution of this file, with or without
 * modification, are permitted in any medium without royalty provided
 * the copyright notice and this notice are preserved.  This file is
 * offered as-is, without any warranty.
 */ 


#include <stdio.h>

int main (void)
  { printf ("hello, world\n")
  ; return 0
  ; }
